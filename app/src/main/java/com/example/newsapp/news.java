package com.example.newsapp;

public class news {

    private String news1;
    private String news2;
    private int portrait;

    public news(String news1, String news2, int portrait) {
        this.news1 = news1;
        this.news2 = news2;
        this.portrait = portrait;
    }



    public String getNews1() {
        return news1;
    }

    public String getNews2() {
        return news2;
    }

    public int getPortrait() {
        return portrait;
    }
}
