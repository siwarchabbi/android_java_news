package com.example.newsapp;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public ImageView imageView;
    public TextView news1;
    public TextView news2;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.image);
        news1 = itemView.findViewById(R.id.news1);
        news2 = itemView.findViewById(R.id.news2);
        itemView.setClickable(true);


    }

    public void bind(news n) {
        news1.setText(n.getNews1());
        news2.setText(n.getNews2());
        imageView.setImageResource(n.getPortrait());
    }
}

