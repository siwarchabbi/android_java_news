package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NewsActivity.OnItemClickListener{
    RecyclerView recyclerView;
    List<news> newsList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView= findViewById(R.id.recycler);
        newsList=new ArrayList<news>();
        newsStack();
        recyclerView.setLayoutManager(new LinearLayoutManager( this));
        NewsActivity adapter=new NewsActivity(newsList);
        adapter.setOnItemClickListener(this);


        recyclerView.setAdapter(adapter);
    }
    private void newsStack() {

        newsList.add(new news("palestine", "palestine news 1", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 2", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 3", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 4", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 5", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 6", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 7", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 8", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 9", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 10", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 11", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 12", R.drawable.r11));
        newsList.add(new news("palestine", "palestine news 13", R.drawable.r12));
        newsList.add(new news("palestine", "palestine news 14", R.drawable.r11));
    }
    @Override
    public void onItemClick(int position) {
        Log.d("MainActivity", "Item clicked at position: " + position);
        news clickedNews = newsList.get(position);
        Intent intent = new Intent(this, DetailNews.class);
        intent.putExtra("title", clickedNews.getNews1());
        intent.putExtra("description", clickedNews.getNews2());
        intent.putExtra("image", clickedNews.getPortrait());
        startActivity(intent);
    }
}