package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailNews extends AppCompatActivity {
    TextView titleTextView;
    TextView descriptionTextView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news2);
        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String description = intent.getStringExtra("description");
        int imageResource = intent.getIntExtra("image", R.drawable.r11);
        titleTextView = findViewById(R.id.titleTextView);
        descriptionTextView = findViewById(R.id.descriptionTextView);
        imageView = findViewById(R.id.imageView);


        titleTextView.setText(title);
        descriptionTextView.setText(description);
        imageView.setImageResource(imageResource);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_send_email) {
            sendEmail();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendEmail() {
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setData(Uri.parse("mailto:"));
        i.putExtra(Intent.EXTRA_SUBJECT, titleTextView.getText().toString());
        String newsDescription = getIntent().getStringExtra("description");
        if (newsDescription != null) {

            i.putExtra(Intent.EXTRA_TEXT, newsDescription);
        }
        if (i.resolveActivity(getPackageManager()) != null) {
            startActivity(i);
        } else {

            Toast.makeText(getApplicationContext(), "No email app installed", Toast.LENGTH_SHORT).show();

        }}
    }


